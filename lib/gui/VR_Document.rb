
class VR_Document < Gtk::SourceView

	include VR_TextViewCommon
	attr_accessor :full_path_file

	def initialize(full_path_file, title_label, main)
		@main = main
		super()
		@title = title_label
		reload_from_disk(full_path_file)
		signal_connect("visibility_notify_event") { scroll_to_cursor() }
		hilight_current_line = true
		self.show_right_margin = true
		self.right_margin_position = 80
		self.auto_indent = true
		self.insert_spaces_instead_of_tabs = true
		self.show_line_numbers = true
		show_line_markers = true
		smart_home_end = true
		tabs_width = 2
		self.indent_width = 2
		lang = Gtk::SourceLanguageManager.new.get_language(get_langauge(full_path_file))
		buffer.language = lang
		buffer.highlight_syntax = true
		self.wrap_mode = Gtk::TextTag::WRAP_WORD
		buffer.create_tag("hilight",	{ "background" => "#FFF0A0" })
		buffer.create_tag("hilight_red", { "background" => "#FFC0C0" })
		buffer.signal_connect("changed") { remove_tag("hilight") }
		update_style()
	end

	def get_langauge(fn)
		case File.extname(full_path_file)
			when ".rb", "", ".gemspec" ; return "ruby"
			when ".erb", ".html" ; return "html"
			else ; return "ruby"
		end
	end

	def update_style()
		modify_font(Pango::FontDescription.new("Monospace"))
		tab_array = Pango::TabArray.new(1,true)
		tab_array.set_tab(0,Pango::TAB_LEFT, 2 * 8)
		set_tabs(tab_array)
	end

	def reload_from_disk(fn = @full_path_file)
		@full_path_file = fn
		@title.text = File.basename(@full_path_file)
		buffer.text = File.open(@full_path_file,"r").read if File.file?(@full_path_file.to_s)
		buffer.modified = false
		@modified_time = mod_time()
	end

	def write_to_disk(fn = @full_path_file)
		@full_path_file = fn
		File.open(fn,"w") { |f| f.print(buffer.text) }
		buffer.modified = false
		@modified_time = mod_time()
		@title.text = File.basename(fn)
		return true
	end

	def mod_time()
		return File.file?(@full_path_file) ? File.stat(@full_path_file).mtime : nil
	end
end
