
class VR_DocumentManager
	def initialize(main,notebook)
		@main = main
		@notebook = notebook
		@pages = {}
		@untitled = -1
	end

	def remove_tab(file)
		controls = @pages.delete(file)
		num = @notebook.page_num(controls[:tab])
		@notebook.remove_page(num)
		@untitled -= 1 if File.basename(file) =~ /Untitled\d?\.rb/
	end
	private :remove_tab

	def add(file="Untitled")
		if file == "Untitled"
			if @untitled == -1
				file = File.join(Dir.pwd,"Untitled.rb")
				@untitled += 1
			else
				file = File.join(Dir.pwd,"Untitled#{@untitled += 1}.rb")
			end
		end
		return if switch_to(file)
		label = VR_TabLabel.new(nil,File.basename(file))
		doc = VR_Document.new(file,label,@main)
		tab = Gtk::ScrolledWindow.new
		tab.add(doc)
		tab.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_ALWAYS)
		@notebook.append_page(tab,label)
		@notebook.show_all
		@pages[file] = {:tab=>tab,:doc=>doc,:label=>label}

		label.signal_connect("close_clicked") do |widget|
			if @pages[file][:doc].buffer.modified?
				d = @main.builder["promptsave"]
				resp = d.run
				d.hide
				if resp == 2 # Save Changes
					save_document(file)
					remove_tab(file)
				elsif resp == 1 # Don't save the Changes
					remove_tab(file)
				else # Cancelled closing action
					# Don't do anything here, just ignore the closing request.
				end
			else
				remove_tab(file)
			end
		end
		switch_to(file)
	end

	def switch_to(file)
		if @pages.has_key? file
			num = @notebook.page_num(@pages[file][:tab])
			@notebook.set_page(num)
			true
		else
			false
		end
	end

	def save_document(file,save_as = false)
		if @pages.has_key? file
			# We're saving the file
			if File.basename(file) =~ /Untitled\d?\.rb/ || save_as
				# It's an Untitled document, or save as action, prompt the user for the filename
				dialog = Gtk::FileChooserDialog.new("Save File As...",
													@main.builder["window1"],
													Gtk::FileChooser::ACTION_SAVE,
													nil,
													[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
													[Gtk::Stock::SAVE, Gtk::Dialog::RESPONSE_ACCEPT])
				dialog.current_folder = File.dirname(file)
				dialog.current_name = File.basename(file)
				resp = dialog.run
				dialog.hide
				if resp == Gtk::Dialog::RESPONSE_ACCEPT
					@pages[file][:doc].write_to_disk(dialog.filename)
					return true
				end
				return false
			else
				@pages[file][:doc].write_to_disk()
			end
		end
	end

	def save_current(save_as = false)
		tab = @notebook.get_nth_page(@notebook.page)
		@pages.each do |file,gtk|
			if gtk[:tab] == tab
				save_document(file,save_as)
			end
		end
	end
				
end
