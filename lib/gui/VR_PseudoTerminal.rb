
class VR_PseudoTerminal < Gtk::TextView
	def initialize
		super()
		self.modify_base(Gtk::STATE_NORMAL,Gdk::Color.new(0,0,0))
		self.modify_text(Gtk::STATE_NORMAL,Gdk::Color.new(65535,65535,65535))
		self.wrap_mode = Gtk::TextTag::WRAP_WORD
		self.modify_font(Pango::FontDescription.new("Courier New, Monospace, 10"))
	end
end
