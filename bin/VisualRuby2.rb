
class VisualRuby2 #(change name)

	attr_reader :builder
	include GladeGUI

	def show()
		load_glade(__FILE__)  #loads file, glade/MyClass.glade into @builder

		set_glade_all(self) #populates glade controls with insance variables (i.e. Myclass.var1) 
		@doc_manager = VR_DocumentManager.new(self,@builder["VR_Documents"])
		@doc_manager.add()
		if AVAILABLE_FEATURES[:vte]
			@output = Vte::Terminal.new
			@builder["VR_OutputHolder"].add(@output)
		else
			@output = VR_PseudoTerminal.new
			@builder["VR_OutputHolder"].add(@output)
		end
		show_window()
	end	

	#region Start of code for Event Handlers

	#region Menu Handlers
	def VR_MenuNewProject__activate(*args)
		dlg = NewProjectDialog.new(self)
		dlg.show(self)
	end

	def VR_MenuNewFile__activate(*args)
		@doc_manager.add()
	end

	def VR_MenuOpen__activate(*args)
		dialog = Gtk::FileChooserDialog.new("Open a File or Project...",
									@builder["window1"],
									Gtk::FileChooser::ACTION_OPEN,
									nil,
									[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
									[Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		filter = Gtk::FileFilter.new
		filter.add_pattern("*.rbproj")
		filter.add_pattern("*.rb")
		filter.add_pattern("*.yaml")
		filter.add_pattern("*.rhtml")
		filter.add_pattern("*.html")
		filter.add_pattern("*")
		dialog.set_filter(filter)
		dialog.current_folder = Dir.pwd
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			@doc_manager.add(dialog.filename)
		end
		dialog.hide
	end

	def VR_MenuSave__activate(*args)
		@doc_manager.save_current()
	end

	def VR_MenuSaveAs__activate(*args)
		@doc_manager.save_current(true)
	end
	
	def VR_MenuQuit__activate(*args)
		destroy_window()
	end
	#endregion

	#region Toolbar Handlers
	def VR_ToolNew__clicked(*args)
		@doc_manager.add()
	end

	def VR_ToolOpen__clicked(*args)
		dialog = Gtk::FileChooserDialog.new("Open a File or Project...",
									@builder["window1"],
									Gtk::FileChooser::ACTION_OPEN,
									nil,
									[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
									[Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		filter = Gtk::FileFilter.new
		filter.add_pattern("*.rbproj")
		filter.add_pattern("*.rb")
		filter.add_pattern("*.yaml")
		filter.add_pattern("*.rhtml")
		filter.add_pattern("*.html")
		filter.add_pattern("*")
		dialog.set_filter(filter)
		dialog.current_folder = Dir.pwd
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			@doc_manager.add(dialog.filename)
		end
		dialog.hide
	end

	def VR_ToolSave__clicked(*args)
		@doc_manager.save_current()
  end

	def VR_ToolSaveAs__clicked(*args)
		@doc_manager.save_current(true)
	end
	#endregion

	#endregion End of code for Event Handlers

end
