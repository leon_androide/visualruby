
class NewProjectDialog #(change name)

	include GladeGUI
	def initialize(main)
		@main = main
	end

	def ColorParse(red,green,blue)
		a = blue + ( green * 256) + ( red * 256 * 256)
		Gdk::Color.parse(sprintf("#%06x",a))
	end

	def show(parent)
		load_glade(__FILE__,parent)

		@builder["project_type_desc"].modify_base(Gtk::STATE_NORMAL,ColorParse(210,210,210))
		@builder["project_type_desc"].set_cursor_visible(false)
		set_glade_all() #populates glade controls with insance variables
		show_window()
	end

end
